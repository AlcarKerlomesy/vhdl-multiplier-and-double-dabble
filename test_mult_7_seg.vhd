library IEEE, std;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_textio.all ;
use std.textio.all ;

entity test_mult_7_seg is
--  Port ( );
end test_mult_7_seg;

architecture Behavioral of test_mult_7_seg is
    component Mult_7_seg
        Generic (
            IS_TEST : std_logic := '0';
            input_size : integer := 8);
        Port (
            input_1 : in unsigned (input_size-1 downto 0);
            input_2 : in unsigned (input_size-1 downto 0);
            start : in STD_LOGIC;
            reset : in STD_LOGIC;
            clk : in STD_LOGIC;
            product : out unsigned (2*input_size-1 downto 0);
            enable_seg : out STD_LOGIC_VECTOR (3 downto 0);
            seg_out : out STD_LOGIC_VECTOR (6 downto 0);
            dot : out STD_LOGIC);
    end component;
    
    constant input_size : integer := 8 ;
    signal input_1 : unsigned (input_size-1 downto 0) := "00000011" ;
    signal input_2 : unsigned (input_size-1 downto 0) := "00000011" ;
    signal start : STD_LOGIC := '0' ;
    signal reset : STD_LOGIC := '0' ;
    signal clk : STD_LOGIC := '0' ;
    signal product : unsigned (2*input_size-1 downto 0);
    signal enable_seg : STD_LOGIC_VECTOR (3 downto 0);
    signal seg_out : STD_LOGIC_VECTOR (6 downto 0) ;
    signal dot : STD_LOGIC ;
    
    constant num_7_seg : integer := 4 ;
    signal all_seg : std_logic_vector (7*num_7_seg-1 downto 0) := (others => '0') ;
    signal last_digit : unsigned (3 downto 0) := (others => '0') ;
begin
    tested_comp: Mult_7_seg
        generic map (IS_TEST => '1', input_size => 8)
        port map(input_1 => input_1, input_2 => input_2, start => start, reset => reset, clk => clk,
            product => product, enable_seg => enable_seg, seg_out => seg_out, dot => dot) ;
    
    clk_proc : process
    begin
        wait for 5 ns ;
        clk <= not clk ;
        wait for 5 ns ;
    end process clk_proc ;
    
    test_proc : process
        file test_file : text ;
        variable t_line : line ;
        variable t_input : std_logic_vector (input_size-1 downto 0) ;
        variable t_output : std_logic_vector (2*input_size-1 downto 0) ;
        variable t_digit : std_logic_vector (3 downto 0) ;
        variable seg_digit : integer range -1 to 9 ;
        variable m2 : integer ;
    begin
        report "Start simulation" severity NOTE ;
        file_open(test_file, "test_file.txt", read_mode) ;
        
        while not endfile(test_file) loop
            readline(test_file, t_line) ;
            
            hread(t_line, t_input) ;
            input_1 <= unsigned(t_input) ;
            hread(t_line, t_input) ;
            input_2 <= unsigned(t_input) ;
            wait for 20 ns ;
            
            start <= '1' ;
            wait for 60 ns ;
            start <= '0' ;
            wait for 600 ns ;
            
            hread(t_line, t_output) ;
            assert unsigned(t_output) = product report
                "Inputs are " & integer'image(to_integer(input_1)) &
                " and " & integer'image(to_integer(input_2)) &
                ". The output is " & integer'image(to_integer(product)) &
                ", but it should be " & integer'image(to_integer(unsigned(t_output)))
                severity FAILURE ;
                
            hread(t_line, t_digit) ;
            assert unsigned(t_digit) = last_digit report
                "The product is " & integer'image(to_integer(product)) &
                ", but the last digit is " & integer'image(to_integer(unsigned(last_digit)))
                severity FAILURE ;
                
            for m in 0 to 3 loop
                m2 := 3 - m ;
                hread(t_line, t_digit) ;
                case all_seg(7*m2+6 downto 7*m2) is
                    when "0111111" => seg_digit := 0 ;
                    when "0000110" => seg_digit := 1 ;
                    when "1011011" => seg_digit := 2 ;
                    when "1001111" => seg_digit := 3 ;
                    when "1100110" => seg_digit := 4 ;
                    when "1101101" => seg_digit := 5 ;
                    when "1111101" => seg_digit := 6 ;
                    when "0000111" => seg_digit := 7 ;
                    when "1111111" => seg_digit := 8 ;
                    when "1101111" => seg_digit := 9 ;
                    when others => seg_digit := -1 ;
                end case ;
                
                assert to_integer(unsigned(t_digit)) = seg_digit report
                    "The product is " & integer'image(to_integer(product)) &
                    ", but the digit " & integer'image(m2) &
                    " is " & integer'image(seg_digit)&
                    "The digit 3 is the left-most one and digit 0 is the right-most one."
                    severity FAILURE ;
            end loop ;
            
            wait for 320 ns ;
        end loop ;
        
        file_close(test_file) ;
        report "End of the simulation" severity NOTE ;
        std.env.finish ;
    end process test_proc ;
    
    seg_proc : process(enable_seg, seg_out)
    begin
        all_seg <= all_seg ;
        last_digit <= last_digit ;
        for m in 0 to num_7_seg-1 loop
            if (enable_seg(m) = '0') then
                all_seg(7*m+6 downto 7*m) <= not seg_out ;
                last_digit(m) <= not dot ;
            end if ;
        end loop ;
    end process seg_proc ;
    
end Behavioral;
