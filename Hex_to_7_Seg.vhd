library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Hex_to_7_Seg is
    Port ( hex : in STD_LOGIC_VECTOR (3 downto 0);
           seven_seg : out STD_LOGIC_VECTOR (6 downto 0));
end Hex_to_7_Seg;

architecture Behavioral of Hex_to_7_Seg is
    signal seg_out : std_logic_vector (6 downto 0) ;
begin
    seven_seg <= not seg_out ;
    
    seg_proc: process(hex)
    begin
        case hex is
            when "0000" => seg_out <= "0111111" ;   --0
            when "0001" => seg_out <= "0000110" ;   --1
            when "0010" => seg_out <= "1011011" ;   --2
            when "0011" => seg_out <= "1001111" ;   --3
            when "0100" => seg_out <= "1100110" ;   --4
            when "0101" => seg_out <= "1101101" ;   --5
            when "0110" => seg_out <= "1111101" ;   --6
            when "0111" => seg_out <= "0000111" ;   --7
            when "1000" => seg_out <= "1111111" ;   --8
            when "1001" => seg_out <= "1101111" ;   --9
            when "1010" => seg_out <= "1110111" ;   --A
            when "1011" => seg_out <= "1111100" ;   --b
            when "1100" => seg_out <= "0111001" ;   --C
            when "1101" => seg_out <= "1011110" ;   --d
            when "1110" => seg_out <= "1111001" ;   --E
            when "1111" => seg_out <= "1110001" ;   --F
            when others => seg_out <= (others => 'X') ;
        end case ;
    end process seg_proc ;

end Behavioral;
