library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Mult_7_seg is
    Generic (
        IS_TEST : std_logic := '0';
        input_size : integer := 8);
    Port (
        input_1 : in unsigned (input_size-1 downto 0);
        input_2 : in unsigned (input_size-1 downto 0);
        start : in STD_LOGIC;
        reset : in STD_LOGIC;
        clk : in STD_LOGIC;
        product : out unsigned (2*input_size-1 downto 0);
        enable_seg : out STD_LOGIC_VECTOR (3 downto 0);
        seg_out : out STD_LOGIC_VECTOR (6 downto 0);
        dot : out STD_LOGIC);
end Mult_7_seg;

architecture Behavioral of Mult_7_seg is
    Component DDA_step
        Port ( digit_in : in unsigned (3 downto 0);
               carry_in : in STD_LOGIC;
               digit_out : out unsigned (3 downto 0);
               carry_out : out STD_LOGIC);
    end component;
    
    component Hex_to_7_Seg
        Port ( hex : in STD_LOGIC_VECTOR (3 downto 0);
               seven_seg : out STD_LOGIC_VECTOR (6 downto 0));
    end component;
    
    component Simple_Clk
        Generic (
            max_count : integer) ;   -- the clock frequency is 100 MHz / max_count
        Port ( clk_in : in STD_LOGIC;
               reset : in STD_LOGIC;
               clk_out : out STD_LOGIC);
    end component;

    -- Clock
    constant clk_freq : integer := 100000000 ;
    constant seven_seg_freq : integer := 800 ;
    constant seven_seg_maxcount : integer := clk_freq/seven_seg_freq ;

    -- State
    type state_type is (init, load, mult, dda, done) ;
    signal state : state_type := init ;
    signal nxt_state : state_type ;
    
    -- Shift and dda counter
    constant maxcount_mult : integer := input_size-1 ;
    constant maxcount_dda : integer := 2*input_size-1 ;
    signal counter : integer range 0 to maxcount_dda := 0 ;
    signal nxt_counter : integer range 0 to maxcount_dda ;
    
    -- To reset when input changes
    signal prev_input_1 : unsigned (input_size-1 downto 0) := (others => '0') ;
    signal prev_input_2 : unsigned (input_size-1 downto 0) := (others => '0') ;
    
    -- Output register
    signal product_reg : unsigned (2*input_size-1 downto 0) := (others => '0') ;
    signal nxt_product_reg : unsigned (2*input_size-1 downto 0) ;
    
    -- DDA register
    constant num_digit : integer := 5 ;
    signal product_dda : unsigned (2*input_size-1 downto 0) := (others => '0') ;
    signal nxt_product_dda : unsigned (2*input_size-1 downto 0) ;
    signal bcd : unsigned (num_digit*4-1 downto 0) := (others => '0') ;
    signal bcd_temp : unsigned (num_digit*4-1 downto 0) := (others => '0') ;
    signal nxt_bcd : unsigned (num_digit*4-1 downto 0) ;
    signal carry : unsigned (num_digit downto 0) := (others => '0') ;
    
    -- Seven segment
    constant num_7_seg : integer := 4 ;
    signal displayed_bcd : unsigned (3 downto 0) := (others => '0') ;
    signal enable_seg_reg : std_logic_vector (num_7_seg-1 downto 0) := "1110" ;
    signal seg_clk : std_logic := '0' ;
begin

    product <= product_reg ;
    
    carry(0) <= product_dda(2*input_size-1) ;
    g1: for m in 0 to num_digit-1 generate
        dda: DDA_step
            port map (digit_in => bcd(4*m+3 downto 4*m), carry_in => carry(m),
                digit_out => bcd_temp(4*m+3 downto 4*m), carry_out => carry(m+1)) ;
    end generate ;
    
    clk_proc : process(clk)
    begin
        if (rising_edge(clk)) then
            prev_input_1 <= input_1 ;
            prev_input_2 <= input_2 ;
        
            counter <= 0 ;
            product_reg <= (others => '0') ;
            product_dda <= (others => '0') ;
            bcd <= (others => '0') ;
            
            if (reset = '1') or (input_1 /= prev_input_1) or (input_2 /= prev_input_2) then
                state <= init ;
            elsif (start = '1') and (state = init) then
                state <= load ;
            else
                state <= nxt_state ;
                counter <= nxt_counter ;
                product_reg <= nxt_product_reg ;
                product_dda <= nxt_product_dda ;
                bcd <= nxt_bcd ;
            end if ;
        end if ;
    end process clk_proc ;
    
    state_machine: process(state, counter, product_reg, product_dda, bcd_temp, bcd, input_1, input_2)
        variable sum : unsigned (input_size downto 0) ;
        variable product_var : unsigned (2*input_size-1 downto 0) ;
    begin
        nxt_state <= init ;
        nxt_counter <= 0 ;
        nxt_product_reg <= (others => '0') ;
        nxt_product_dda <= (others => '0') ;
        nxt_bcd <= (others => '0') ;
    
        case state is
            when load =>
                nxt_product_reg(input_size-1 downto 0) <= input_2 ;
                nxt_product_reg(2*input_size-1 downto input_size) <= (others => '0') ;
                
                nxt_state <= mult ;
                
            when mult =>
                if (product_reg(0) = '1') then
                    sum := ('0' & product_reg(2*input_size-1 downto input_size)) + ('0' & input_1) ;
                    product_var := sum & product_reg(input_size-1 downto 1) ;
                else
                    product_var := '0' & product_reg(2*input_size-1 downto 1) ;
                end if ;
            
                nxt_product_reg <= product_var ;
            
                if (counter = maxcount_mult) then
                    nxt_state <= dda ;
                    nxt_product_dda <= product_var ;
                else
                    nxt_state <= mult ;
                    nxt_counter <= counter + 1 ;
                end if ;
                
            when dda =>
                nxt_product_dda <= product_dda(2*input_size-2 downto 0) & '0' ;
                nxt_bcd <= bcd_temp ;
                
                nxt_product_reg <= product_reg ; 
                if (counter = maxcount_dda) then
                    nxt_state <= done ;
                else
                    nxt_state <= dda ;
                    nxt_counter <= counter + 1 ;
                end if ;
                
            when done =>
                nxt_product_reg <= product_reg ;
                nxt_bcd <= bcd ;
                nxt_state <= done ;
                
            when others =>
                null ;
        end case ;
    end process state_machine ;
    
    ------ Seven segment ------
    hex_to_seg: Hex_to_7_Seg
        port map (hex => std_logic_vector(displayed_bcd), seven_seg => seg_out) ;
    
    v_clk: Simple_Clk
        generic map (max_count => seven_seg_maxcount)
        port map (clk_in => clk, reset => reset, clk_out => seg_clk) ;
        
    enable_seg <= enable_seg_reg ;
    
    seg_clk_proc : process(clk)
    begin
        if (rising_edge(clk)) then
            if (seg_clk = '1') or (IS_TEST = '1') then
                enable_seg_reg <= enable_seg_reg(num_7_seg-2 downto 0) & enable_seg_reg(num_7_seg-1) ;
            end if ;
            if (reset = '1') then
                enable_seg_reg <= "1110" ;
            end if ;
        end if ;
    end process seg_clk_proc ;
    
    seg_proc: process(enable_seg_reg, bcd)
    begin
        dot <= '1' ;
        displayed_bcd <= (others => '0') ;
        for m in 0 to num_7_seg-1 loop
            if (enable_seg_reg(m) = '0') then
                displayed_bcd <= bcd(4*m+3 downto 4*m) ;
                dot <= not bcd(4*num_7_seg + m) ;
            end if ;
        end loop ;
    end process seg_proc ;
end Behavioral;
