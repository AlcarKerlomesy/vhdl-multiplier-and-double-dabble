library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity DDA_step is
    Port ( digit_in : in unsigned (3 downto 0);
           carry_in : in STD_LOGIC;
           digit_out : out unsigned (3 downto 0);
           carry_out : out STD_LOGIC);
end DDA_step;

architecture Behavioral of DDA_step is
begin

    dda_proc: process(digit_in, carry_in)
    begin
        digit_out(0) <= carry_in ;
        if (digit_in > "0100") then
            digit_out(3 downto 1) <= digit_in(2 downto 0) + 3 ;
            carry_out <= '1' ;
        else
            digit_out(3 downto 1) <= digit_in(2 downto 0) ;
            carry_out <= '0' ;
        end if ;
    end process dda_proc ;

end Behavioral;
